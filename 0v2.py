rules = "\033[4mSpeiderne\033[0m\n- a card game by Samuel Brox.\nIt's a game where you will start expeditions, fight pirates, find treasures, and, eventually, building your own ferries; and all with the use of only a 52-card deck, a pen and some paper! (Magic, isn't it?)\n Players: greater than 1.\n What you need: 1 deck of playing cards without jokers, a pen and a piece of paper to write down points on (if you are more than 3, it is recommended you use more than 1 deck).\n Values of cards: 2 is lowest, A is highest.\n\033[4mGoal\033[0m\nThe game consists of two stages: 1 - Discoverer (not yet implemented), 2 - Ferries. The goal is to get the most points, which you get by the number of discovered regions, and later, for number of passangers.\n\n\033[4mDiscoverer\nBefore you start\033[0m\nMake a 3⨯4 area with the value hidden: each of those cards represent an undiscovered region. Then give out 5 cards to each player. Lay out another 3 cards, which will be the " + '"insurance" with the card values shown. The rest of the cards are placed with the values hidden, and will be the "main deck". If, at one point in the game, the main deck is out of cards, you should shuffle the discard and refill with that. When you have decided who goes first, you' + "'re ready to play.\n\033[4mYour turn\033[0m\nOn your turn you can do one of these following things:\n 1. Draw cards\n 2. Make ready an expedition\n 3. Move an expedition\nAfter that, the turn passes to the next player.\n\033[4mDraw cards\033[0m\nYou could draw:\n 1. 2 cards from the main deck\n 2. 1 card from the insurance, unless on your last turn you lost an expedition, in which case you could take 2 cards (that's why it's called " + '"insurance").\n\033[4mMake ready an expedition\033[0m\nTake 3 cards of the same sort from your hand and put it on besides a region, on a place not occupied. That will be your expedition. Don' + "'t forget passing the turn to the next player! After moving this expedition for the first time, it has to be funded at the end of each turn by any card from the owner's hand (that means throwing that card to the discard pile). If this is not done, one card from the expedition is trown away instead.\n\033[4mMove an expedition\033[0m\nChoose a bordering region (up, down, left or right). If the region has been discovered previously (card has the face up), then the expedition cards are just moved to the new region, nothing happens and the turn is over. If the region is undiscovered (card has the face down), then turn the card and show the value. Different things may happen based on the suit and value of the card turned. If there is any expedition left after the actions below, the expedition is placed on top of the newly discovered region (the card that is now with it's face up). If all the expedition is lost, then the region is to be considered as still undiscovered. The region card that everyone has seen is to be discarded and replaced with a new card with the face down.\n\n♣ - Storm\nIf there's not enough resources to save the expedition, it might get sunk! There's 2 cards that define the strength of the storm and that you need to defend against: the storm-card itself and 1 card from the main deck. On each of these cards you should throw something higher than it from your hand. For each card you can't defend against, you take one card away from your expedition. All the used cards (what you defended with from your hand, what you took away from your expedition, the one from the main deck, however, the storm-card itself is to be left on the " + '"map") are thrown away to the discard pile.\n\n♠ - Windless\nWhat a pity: no wind! You will have to fund this expedition with 2 cards, instead of 1 this turn.\n\n♥ - Pirates\nWe encounter an island! And then we see pirates... These pirates plunder your expedition: they won' + "'t stop until they get enough. Take cards from the expedition until the sum of the cards is more than the value of the pirate-card. These cards go to the discard pile. Same as the storm, the used cards (what you took away from your expedition, however, the pirate-card itself is to be left on the " + '"map") are thrown away to the discard pile.\n\n♦ - Treasure\nAn island! Seems like there should be treasure here... However, that doesn' + "'t always end well. If the island is too big (value of the treasure-card is more than 5), you loose a part of your expedition (throw a card to the discard pile). But you get your treasure, though! (take one card from the main deck)\n\nPoints\nYou get 1 point for each region you discover."
import random
cards = [["🃒", "🃓", "🃔", "🃕", "🃖", "🃗", "🃘", "🃙", "🃚", "🃛", "🃝", "🃞", "🃑"], ["🂢", "🂣", "🂤", "🂥", "🂦", "🂧", "🂨", "🂩", "🂪", "🂫", "🂭", "🂮", "🂡"], ["🂲", "🂳", "🂴", "🂵", "🂶", "🂷", "🂸", "🂹", "🂺", "🂻", "🂽", "🂾", "🂱"], ["🃂", "🃃", "🃄", "🃅", "🃆", "🃇", "🃈", "🃉", "🃊", "🃋", "🃍", "🃎", "🃁"]]

def indexes(matrix, item):
  return [(i, j) for i in range(len(matrix)) for j in range(len(matrix[i])) if matrix[i][j] == item]
def fillup(deck, bunke=False, change_list=True):
  adding = []
  if not bunke:
    for l in cards:
      adding.extend(l)
    random.shuffle(adding)
    if change_list:
      deck.extend(adding)
    else:
      return adding
  else:
    random.shuffle(bunke)
    adding.extend(bunke)
    bunke.clear()

class Expedition:
  def __init__(self, location, owner, cards):
    self.l = location
    self.O = owner
    self.c = cards

class Game:
  def __init__(self, PLAYERS, namelist=[]):
    self.a = [[], [], []]
    self.d = []
    self.e = []
    self.explocs = []
    self.h = []
    self.m = []
    fillup(self.m)
    self.i = [self.m.pop(0), self.m.pop(0), self.m.pop(0)]
    self.moved = []
    self.n = namelist
    if not self.n:
      for n in range(PLAYERS):
        self.n.append(f"Player{n + 1}")
    self.P = PLAYERS
    self.p = [] # points
    self.storm = []
    for player in range(self.P):
      self.p.append(0)
      self.moved.append(0)
      self.storm.append(0)
    self.s = ["", 0, []]
    self.t = 0
    self.u = [(1, 1), (1, 2), (1, 3), (1, 4), (2, 1), (2, 2), (2, 3), (2, 4), (3, 1), (3, 2), (3, 3), (3, 4)]
    for row in self.a:
      for number in range(4):
        row.append(self.m.pop(0))
    for player in range(PLAYERS):
      self.h.append([])
    for number in range(5):
      while len(self.m) < PLAYERS:
        fillup(self.m)
      for player in range(PLAYERS):
        self.h[player].append(self.m.pop(0))
    if len(self.m) < 3:
      fillup(self.m)

  def area(self):
    print("  0 1 2 3 4 5")
    for row in range(5):
      print(row, end=" ")
      for number in range(6):
        printed = False
        if row not in (0, 4) and number not in (0, 5):
          if (row, number) not in self.u:
            print(self.a[row - 1][number - 1], end="")
            if (row, number) in self.explocs:
              print("e", end="")
            else:
              print(end=" ")
          else:
            print("🂠", end=" ")
        else:
          if (row, number) in self.explocs:
            print("e", end=" ")
          else:
            print(" ", end=" ")
      print()

  def ins(self):
    print(" 1 2 3")
    print(f" {self.i[0]} {self.i[1]} {self.i[2]}")

  def md(self):
    for player in range(self.P):
      multiple = "s" if len(self.h[player]) > 1 else ""
      playerturn = " •••" if self.t == player else ""
      print(f"{self.n[player]}: {len(self.h[player])} card{multiple}{playerturn}")
    number = 1
    for card in self.h[self.t]:
      print(f"{number}) {card}")
      number += 1

  def situation(self):
    if self.s[0] == "":
      return "It's your turn. You could:\n 1. Draw 2 cards from the main deck;\n 2. Draw 1 (or 2, if you lost your only expedition last turn) card(s) from the 'insurance';\n 3. Prepare an expedition;\n 4. Move an expedition."
    elif False: # This is reserved for a multi-device version
      return f"You have to wait for your turn, since right now it is the turn of {self.n[self.t]}."
    elif self.s[0] == "funding ":
      return "Before ending your turn, you have to fund each of your expeditions for them not to sink. Type the index of any card from your to use it."
    elif self.s[0] == "storm ":
      return f'Storm! Type the index of the card you want to use from your hand. By typing "sink", you loose the lowest card from your expedition. The cards you have to defend against: {self.s[1][0]} {self.s[1][1]} .'
    elif self.s[0] == "pirates ":
      multiple = "" if len(self.s[2]) == 1 else "s"
      cards_used = ""
      value_of_cards_used = 0
      for card in self.s[2]:
        cards_used += f"{card} "
        value_of_cards_used += cards.value(card)
      return f"Pirates! Type the index of the card you want to use from your expedition. The pirate-card is {self.s[1]} . You have used {len(self.s[2])} card{multiple} ({cards_used}; {value_of_cards_used} < {indexes(cards, self.s[1])[0][1]})."

  def points(self):
    for player in range(self.P):
      multiple = "" if self.p[player] == 1 else "s"
      print(f"{self.n[player]}: {self.p[player]} point{multiple}")

  def next_turn(self):
    if self.s[0] == "":
      self.s[0] = "funding"
      self.s[1] = self.moved[self.t]
      if self.s[1] == 0:
        self.s[0] = ""
        self.t += 1
      self.s[2] = []
    elif self.s[0] == "pirates " and len(self.s[2]) >= self.s[1]:
      self.t += 1
    elif self.s[0] == "storm":
      try_this = [[indexes(cards, self.s[1][0])[0][1], indexes(cards, self.s[1][1])[0][1]], []]
      for card in self.s # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

  def change_order(self, player_name, index1, index2):
    if player_name not in self.n:
      raise IndexError(f'"{player_name}" not in names.')
    self.h[self.n.index[player_name]].insert(index2, self.h[self.n.index(player_name)].pop(index1))

  def draw2(self):
    if self.m < 2:
      fillup(self.m, self.d)
    if self.m < 2:
      fillup(self.m)
    self.h[self.t].extend([self.m.pop(0), self.m.pop(0)])
    self.next_turn()

  def draw_ins(self, card_index):
    if self.m < 1:
      fillup(self.m, self.d)
    if self.m < 1:
      fillup(self.m)
    self.h[self.t].append(self.i.pop(card_index))
    self.i.insert(card_index, self.m.pop(0))
    self.next_turn()

  def prep(self, coordinate, indexes_arg):
    args = sorted(indexes_arg)
    if indexes(cards, self.h[self.t][args[0]])[0][1] != indexes(cards, self.h[self.t][args[1]])[0][1] or indexes(cards, self.h[self.t][args[0]])[0][1] != indexes(cards, self.h[self.t][args[2]])[0][1] or indexes(cards, self.h[self.t][args[1]])[0][1] != indexes(cards, self.h[self.t][args[2]])[0][1]:
      raise IndexError("card sorts do not match.")
    self.e.append(Expedition(coordinate, self.n[self.t], [self.h[self.t].pop(args[2]), self.h[self.t].pop(args[1]), self.h[self.t].pop(args[0])]))
    self.moved[self.t] += 1
    self.next_turn()

  def movexp(self, coordinate1_arg, coordinate2_arg):
    coordinate1, coordinate2 = [tuple(coordinate1_arg), tuple(coordinate2_arg)]

def MAIN():

  while True:
    try:
      PLAYERS = int(input("Players: "))
      if PLAYERS > 1:
        break
      else:
        print("Number of players must be greater than 1.")
    except ValueError:
      print("Please give a valid number of players.")
  names = []
  n = 1

  while n <= PLAYERS:
    names.append(input(f"{n}. "))
    if names[-1] == "":
      names[-1] = f"Player{n}"
    else:
      check = -2
      while -check < len(names) + 1:
        if names[check].upper() == names[-1].upper():
          print("Name already used!")
          n -= 1
          names.pop()
          break
        check -= 1
    n += 1
  Discoverer = Game(PLAYERS, names)
  pastcom = None

  while True:
    done = False
    com = input(f"{names[Discoverer.t]} >> ").lower()

    if pastcom == "md":
      print("\033[A" + " " * (len(Discoverer.n[Discoverer.t]) + 4 + len(com)) + "\033[A")
      for card in range(len(Discoverer.h[Discoverer.t])):
        print("\033[A", end="")
        print(" " * 5)
        print("\033[A", end="")
      print(f"{Discoverer.n[Discoverer.t]} {self.s[0]}>> {com}")

    if com in ("area", "map", "a", "m"):
      Discoverer.area()
      done = True

    if com in ("i", "ins", "insurance"):
      Discoverer.ins()
      done = True

    if com in ("md", "my deck"):
      Discoverer.md()
      done = True

    if com in ("p", "ps", "points", "point"):
      Discoverer.points()
      done = True

    if com in ("rules", "rulebook", "rule"):
      print(rules)
      done = True

    if com in ("h", "help"):
      print('"map" or "area": show area/\033[1mmap/area\033[0m;\n"ins": show ' + "'\033[1mins\033[0murance';\n" + '"md": show \033[1mm\033[0my \033[1md\033[0meck and the number of cards each player has;\n"s": show your current \033[1ms\033[0mituation and what actions you can make;\n"p": show how many \033[1mp\033[0moints each player has;\n"r": show \033[1mr\033[0mules.\n    Actions:\n"draw2": \033[1mdraw two\033[0m cards from the main deck;\n"draw ins {index of the card you want to \033[1mdraw\033[0m from the' + "'\033[1mins\033[0murance'}" + '";\n"prep {\033[48;5;1mindex1\033[0m} {\033[48;5;1mindex2\033[0m} {\033[48;5;1mindex3\033[0m}": \033[1mprep\033[0mare an expedition with \033[48;5;1mthese\033[0m following cards from my deck;\n"move {coordinate1} {coordinate2}": \033[1mmove\033[0m an expedition from \033[48;5;1mcoordinate1\033[0m to \033[48;5;1mcoordinate2\033[0m.')
      done = True

    if com in ("draw2", "draw 2", "d2", "d 2", "dr2", "dr 2", "draw m", "draw main", "draw main deck", "d m", "dm", "drm", "dr m", "d main", "d main deck", "dr main", "dr main deck"):
      Discoverer.draw2()

    if com in ("hide", ""):
      done = True

    if not done:
      print('Not yet implemented. For available commands, type "help". If you think this feature should be implemented, contact \033[38;5;39mSamuel.Brox026@gmail.com\033[0m.')
    pastcom = com

if __name__ == "__main__":
  MAIN()
  
